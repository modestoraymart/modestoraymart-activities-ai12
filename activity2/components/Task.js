import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

const Task = (props) => {
  return (
    <View style={styles.item}>
      <View style={styles.itemLeft}>
        <View style={styles.square}></View>
        <Text style={styles.itemText}>{props.text}</Text>
      </View>
      <View style={styles.circular}>
        <Text>✔</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    backgroundColor: "#07B6C2",
    padding: 15,
    borderRadius: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 20,
  },
  itemLeft: {
    flexDirection: "row",
    alignItems: "center",
    flexWrap: "wrap",
  },
  //Tasks
  square: {
    width: 10,
    height: 15,
    opacity: 1,
    borderRadius: 20,
    marginRight: 3,
  },
  itemText: {
    maxWidth: "80%",
  },
  //check
  circular: {
    width: 25,
    height: 25,
    borderColor: "#81D9DF",
    borderWidth: 2,
    borderRadius: 3,
    alignItems: "center",
    backgroundColor: "#07B6C2",
  },
});

export default Task;
