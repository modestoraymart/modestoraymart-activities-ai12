import React from "react";
import { View, Text, TouchableOpacity, Button, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";

const Home = () => {
  const navigation = useNavigation();
  return (
    <View style={[{ width: "100%", top: 10 }]}>
      <Text
        style={{
          textAlignVertical: "center",
          textAlign: "center",
        }}
      >
        Completed Tasks Go Here
      </Text>
      <Button
        style={{ bottom: 50 }}
        title="To-Do-List App"
        color="#05AEB9"
        onPress={() => navigation.navigate("ToDoApp")}
      ></Button>
    </View>
  );
};

export default Home;
