import React, { useState } from "react";
import ToDoApp from "../activity2/Screens/ToDoApp";
import Home from "../activity2/Screens/Home";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="   " component={Home} />
        <Stack.Screen name="ToDoApp" component={ToDoApp} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
