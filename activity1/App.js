import { useState } from "react";
import { StyleSheet, Button, View, Alert, TextInput } from "react-native";

export default function App() {
  const [input, setInput] = useState("");
  {
    return (
      <View style={styles.container}>
        <TextInput
          input
          Id="text1"
          style={styles.input}
          onChangeText={(value) => setInput(value)}
          placeholder={"Enter Text Here"}
          placeholderTextColor="gray"
        />
        <Button
          title="SUBMIT"
          color="black"
          onPress={() => alert("You just typed:  " + input)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "lightblue",
  },
  input: {
    width: "60%",
    borderColor: "black",
    borderWidth: 2,
    borderRadius: 15,
    padding: 10,
    margin: 20,
  },
});
