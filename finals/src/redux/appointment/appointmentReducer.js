import { ADD_APPOINTMENT, SEARCH_APPOINTMENT } from "./appointmentTypes";

const initialState = {
  appointments: localStorage.getItem("event")
    ? JSON.parse(localStorage.getItem("event"))
    : [],
  selectedAppointment: {},
};
const appointmentReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_APPOINTMENT:
      return {
        ...state,
        appointments: [...state.appointments, action.payload],
      };
    case SEARCH_APPOINTMENT:
      return {
        ...state,
        selectedAppointment: state.appointments.find(
          (item) => item.id === action.payload
        ),
      };
    default:
      return state;
  }
};

export default appointmentReducer;
