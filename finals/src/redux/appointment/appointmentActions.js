import { ADD_APPOINTMENT, SEARCH_APPOINTMENT } from "./appointmentTypes";

export const add_appointment = (appointmentData) => (dispatch) => {
  dispatch({
    type: ADD_APPOINTMENT,
    payload: appointmentData,
  });
  return Promise.resolve();
};
export const search_appointment = (id) => {
  return {
    type: SEARCH_APPOINTMENT,
    payload: id,
  };
};
