import styled from "styled-components";

export const RootContainer = styled.div`
  margin: 0;

  .react-date-picker {
    width: 100%;
  }
`;
