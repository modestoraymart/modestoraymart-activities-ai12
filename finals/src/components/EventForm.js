import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import DatePicker from "react-date-picker";
import TimeRangePicker from "@wojtekmaj/react-timerange-picker";
import moment from "moment";
import { useDispatch } from "react-redux";
import { add_appointment } from "../redux/appointment/appointmentActions";
import uuid from "react-uuid";
import {
  FormWrapper,
  Header,
  ListItem,
  ListContainer,
  AddButton,
  Input,
  Label,
} from "../styles/EventFormStyles";

const EventForm = ({ closeModal }) => {
  const [startDateTime, setStartDateTime] = useState(new Date());
  const [endDateTime, setEndDateTime] = useState(new Date());

  const dispatch = useDispatch();

  const { register, handleSubmit, reset } = useForm();

  const [selectedDate, onChangeDate] = useState(new Date());

  const [selectedTime, onChangeTime] = useState(["10:00", "11:00"]);

  useEffect(() => {
    let formattedDate = moment(selectedDate).format("YYYY-MM-DD");
    setStartDateTime(formattedDate + "T" + selectedTime[0] + ":00");
    setEndDateTime(formattedDate + "T" + selectedTime[1] + ":00");
  }, [selectedDate, selectedTime]);

  const onSubmit = (data) => {
    const appointmentInfo = {
      ...data,
      start: startDateTime,
      end: endDateTime,
      id: uuid(),
    };
    dispatch(add_appointment(appointmentInfo))
      .then(() => {
        console.log("form data: ", appointmentInfo);
        if ("appointments" in localStorage) {
          let localStorageArray = JSON.parse(
            localStorage.getItem("appointments")
          );
          localStorageArray.push(appointmentInfo);
          localStorage.setItem(
            "appointments",
            JSON.stringify(localStorageArray)
          );
        } else {
          let newArray = [];
          newArray.push(appointmentInfo);
          localStorage.setItem("appointments", JSON.stringify(newArray));
        }
        reset();
        setStartDateTime(new Date());
        setEndDateTime(new Date());
        closeModal();
      })
      .catch((error) => {
        console.log(`Error getting data: ${error}`);
        closeModal();
      });
  };

  return (
    <FormWrapper>
      <Header>Adding new Event</Header>
      <form onSubmit={handleSubmit(onSubmit)}>
        <ListContainer>
          <ListItem>
            <Label>Event Name</Label>
            <Input
              name="title"
              type="text"
              placeholder="Type here..."
              required
              {...register("title", {
                required: true,
                maxLength: 100,
              })}
            />
          </ListItem>
          <ListItem>
            <Label>Event Details</Label>
            <Input
              name="name"
              type="text"
              placeholder="Type here..."
              required
              {...register("name", {
                required: true,
                maxLength: 45,
              })}
            />
          </ListItem>
          <ListItem>
            <Label>Date</Label>
            <DatePicker
              onChange={onChangeDate}
              value={selectedDate}
              format="y-MM-d"
            />
          </ListItem>
          <ListItem>
            <Label>Time</Label>
            <TimeRangePicker onChange={onChangeTime} value={selectedTime} />
          </ListItem>
          <ListItem>
            <AddButton type="submit">Add To Your Calendar</AddButton>
          </ListItem>
        </ListContainer>
      </form>
    </FormWrapper>
  );
};

export default EventForm;
