import React from "react";
import { View, Text, TouchableOpacity, Image, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import Fun2 from "../assets/Fun2.png";

const VI = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.AboveText}>{`Makes you look cool`}</Text>
        <Image
          style={{
            resizeMode: "cover",
            height: 450,
            width: "100%",
            top: 30,
            left: 1,
          }}
          source={Fun2}
        />
        <Text
          style={styles.LowerText}
        >{`It just makes you look cool when you mention it to your friends who doesn't know anything about computers.`}</Text>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate("Home")}
        >
          <Text style={styles.ButtonText}>Home</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  image: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    resizeMode: "cover",
  },
  button: {
    flex: 1,
    position: "absolute",
    alignSelf: "center",
    borderRadius: 25,
    top: 610,
    backgroundColor: "#43E5CC",
    padding: 15,
    elevation: 15,
  },
  ButtonText: {
    fontSize: 15,
    color: "white",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase",
  },
  AboveText: {
    textAlign: "center",
    top: 30,
    fontSize: 20,
    textTransform: "uppercase",
    fontWeight: "bold",
    letterSpacing: 1,
    color: "black",
    textShadowColor: "#43E5CC",
    textShadowRadius: 10,
    textShadowOffset: { width: 1, height: 3 },
  },
  LowerText: {
    textAlign: "center",
    top: 20,
    fontSize: 20,
    textTransform: "none",
    fontWeight: "normal",
    letterSpacing: 1,
    lineHeight: 40,
    color: "black",
  },
});

export default VI;
