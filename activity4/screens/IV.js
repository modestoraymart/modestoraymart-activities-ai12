import React from "react";
import { View, Text, TouchableOpacity, Image, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import Career2 from "../assets/Career2.png";

const IV = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.AboveText}>{`A Highly in-demand job`}</Text>
        <Image
          style={{
            resizeMode: "cover",
            height: 450,
            width: "100%",
            top: 60,
            left: 1,
          }}
          source={Career2}
        />
        <Text
          style={styles.LowerText}
        >{`As technology advances day by day, the need for professionals in this field will continue to increase.`}</Text>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate("V")}
        >
          <Text style={styles.ButtonText}>Continue</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  image: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    resizeMode: "cover",
  },
  button: {
    flex: 1,
    position: "absolute",
    alignSelf: "center",
    borderRadius: 25,
    top: 610,
    backgroundColor: "#43E5CC",
    padding: 15,
    elevation: 15,
  },
  ButtonText: {
    fontSize: 15,
    color: "white",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "none",
  },
  AboveText: {
    textAlign: "center",
    top: 30,
    fontSize: 18,
    textTransform: "uppercase",
    fontWeight: "bold",
    letterSpacing: 1,
    color: "black",
    textShadowColor: "#43E5CC",
    textShadowRadius: 10,
    textShadowOffset: { width: 1, height: 3 },
  },
  LowerText: {
    textAlign: "center",
    top: 20,
    fontSize: 22,
    textTransform: "none",
    fontWeight: "normal",
    letterSpacing: 1,
    lineHeight: 30,
    color: "black",
  },
});

export default IV;
