import React from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";
import Profile2 from "../assets/Profile2.png";

const Home = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <View>
        <Text
          style={styles.AboveText}
        >{`Reasons Why I Chose\nInformation Technology`}</Text>
        <Image
          style={{
            resizeMode: "center",
            height: 350,
            width: "100%",
            top: 60,
          }}
          source={Profile2}
        />
        <Text style={styles.LowerText}>{`Modesto Raymart R.\nBSIT AI-12`}</Text>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate("II")}
        >
          <Text style={styles.ButtonText}>Continue</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  button: {
    flex: 1,
    position: "absolute",
    alignSelf: "center",
    borderRadius: 25,
    top: 610,
    backgroundColor: "#43E5CC",
    padding: 15,
    elevation: 15,
  },
  ButtonText: {
    fontSize: 15,
    color: "white",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "none",
  },
  AboveText: {
    textAlign: "center",
    top: 30,
    fontSize: 20,
    textTransform: "uppercase",
    fontWeight: "bold",
    letterSpacing: 1.5,
    color: "black",
    textShadowColor: "#00FFD8",
    textShadowRadius: 10,
    textShadowOffset: { width: 1, height: 3 },
  },
  LowerText: {
    textAlign: "center",
    top: 110,
    fontSize: 15,
    textTransform: "uppercase",
    fontWeight: "bold",
    letterSpacing: 3,
    color: "black",
  },
});

export default Home;
