import React from "react";
import { View, Text, TouchableOpacity, Image, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import Network2 from "../assets/Network2.png";

const III = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <View>
        <Text
          style={styles.AboveText}
        >{` IT encompasses both computer\n technology and telecommunications`}</Text>
        <Image
          style={{
            resizeMode: "cover",
            height: 400,
            width: "100%",
            top: 35,
            left: 1,
          }}
          source={Network2}
        />
        <Text
          style={styles.LowerText}
        >{`It is a course that covers everything there is to know about researching computing technology and installing apps in order to create some 
of the most complex computer networks and databases.`}</Text>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate("IV")}
        >
          <Text style={styles.ButtonText}>Continue</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  image: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    resizeMode: "cover",
  },
  button: {
    flex: 1,
    position: "absolute",
    alignSelf: "center",
    borderRadius: 25,
    top: 610,
    backgroundColor: "#43E5CC",
    padding: 15,
    elevation: 15,
  },
  ButtonText: {
    fontSize: 15,
    color: "white",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "none",
  },
  AboveText: {
    textAlign: "center",
    top: 30,
    fontSize: 18,
    textTransform: "uppercase",
    fontWeight: "bold",
    letterSpacing: 1,
    color: "black",
    textShadowColor: "#43E5CC",
    textShadowRadius: 10,
    textShadowOffset: { width: 1, height: 3 },
  },
  LowerText: {
    textAlign: "center",
    top: 20,
    fontSize: 18,
    textTransform: "none",
    fontWeight: "normal",
    letterSpacing: 1,
    lineHeight: 30,
    color: "black",
  },
});

export default III;
