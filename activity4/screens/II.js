import React from "react";
import { View, Text, TouchableOpacity, Image, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import Education2 from "../assets/Education2.png";

const II = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.AboveText}>{`inexpensive cost of\nEducation`}</Text>
        <Image
          style={{
            resizeMode: "cover",
            height: 450,
            width: "90%",
            top: 25,
            left: 20,
          }}
          source={Education2}
        />
        <Text
          style={styles.LowerText}
        >{`It has a very inexpensive cost of schooling when compared to many other career options.`}</Text>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate("III")}
        >
          <Text style={styles.ButtonText}>Continue</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  image: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    resizeMode: "cover",
  },
  button: {
    flex: 1,
    position: "absolute",
    alignSelf: "center",
    borderRadius: 25,
    top: 610,
    backgroundColor: "#43E5CC",
    padding: 15,
    elevation: 15,
  },
  ButtonText: {
    fontSize: 15,
    color: "white",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "none",
  },
  AboveText: {
    textAlign: "center",
    top: 30,
    fontSize: 20,
    textTransform: "uppercase",
    fontWeight: "bold",
    letterSpacing: 1.5,
    color: "black",
    textShadowColor: "#43E5CC",
    textShadowRadius: 10,
    textShadowOffset: { width: 1, height: 3 },
  },
  LowerText: {
    textAlign: "center",
    top: 25,
    fontSize: 20,
    textTransform: "none",
    fontWeight: "normal",
    letterSpacing: 1,
    lineHeight: 30,
    color: "black",
  },
});

export default II;
