import Home from "./screens/Home";
import II from "./screens/II";
import III from "./screens/III";
import IV from "./screens/IV";
import V from "./screens/V";
import VI from "./screens/VI";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="II" component={II} />
        <Stack.Screen name="III" component={III} />
        <Stack.Screen name="IV" component={IV} />
        <Stack.Screen name="V" component={V} />
        <Stack.Screen name="VI" component={VI} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
